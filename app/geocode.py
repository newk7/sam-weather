import requests, json
import json


map_key = 'AIzaSyC2du7EOAOv1PoqP4FDwDZX2wd7ojo1er4'

def geocode_handler(event, context):

    try:
        queryString = event['queryStringParameters']

        if not ('city' in queryString and 'state' in queryString):
            return {
                'statusCode': 400,
                'body': 'Must include both city and state'
            }
    
        user_city = queryString['city']
        user_state = queryString['state']
        
        # make request to Google Maps API to geocode city and state to lat and long coordinates
        url = 'https://maps.googleapis.com/maps/api/geocode/json?address='+user_city + ',' + user_state +'&key=' + map_key
        print(url)

        r = requests.get(url)
        if r.status_code == 400:
            return {
                'statusCode': 400,
                'body': 'Invalid request to google maps API'
            }
        elif r.status_code == 500:
            return {
                'statusCode': 500,
                'body': 'error from google maps API'
            }

        r = r.json()
        latitude = r.get('results')[0].get('geometry').get('location').get('lat')
        longitude = r.get('results')[0].get('geometry').get('location').get('lng')

        #user_lat, user_long = get_coords(user_city, user_state)
        
        # put coordinates into dictionary to pass to return statement
        coords = {
            "latitude": latitude,
            "longitude": longitude
        }
        return {
            'statusCode': 200,
            'body': json.dumps(coords)
        }
    except:
        return {
            'statusCode': 500,
            'body': 'error executing lambda'
        }
    


# def get_coords(city, state):
    
# # print(get_coords('Portsmouth','NH'))