#! /usr/bin/env python3
import json, requests, random
from datetime import datetime


def healthCheck_handler(event, context):
     code, comment = healthStatus()
     return {
            'statusCode': code,
            'body': comment
        }

def healthStatus():
    rand_num = random.randint(1,100)

    if rand_num % 2 == 0:
        return 200, 'Healthy'
    else:
        return 500, 'Unhealthy'
